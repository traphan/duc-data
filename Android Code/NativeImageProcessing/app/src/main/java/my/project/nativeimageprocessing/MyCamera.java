package my.project.nativeimageprocessing;

/**
 * Created by ducdang on 10/24/16.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import org.opencv.android.OpenCVLoader;
import my.project.nativeimageprocessing.colorCorrection.ImageCapturingActivity;

@SuppressWarnings("deprecation")
public class MyCamera extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    private CameraPreview camPreview;
    private FrameLayout cameraView;
    public int CameraFrameWidth;
    public int CameraFrameHeight;
    private Button buttonStop;
    private Button buttonRestart;
    private Button buttonTakePicture;

    private SurfaceView camView;
    private SurfaceHolder camHolder;
    private static final String TAG = "MyCamera";
    private Handler stoppingHandler = new Handler();

    static {
        System.loadLibrary("imageProcessing");
        if (!OpenCVLoader.initDebug()){
            Log.d(TAG,"OpenCV loaded");
        }else{
            Log.d(TAG,"OpenCV not loaded");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermission();

        CameraFrameWidth = getResources().getInteger(R.integer.camera_frame_size_width);
        CameraFrameHeight = getResources().getInteger(R.integer.camera_frame_size_height);
        setupSharedPreferences();
        showCameraPreview();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregister VisualizerActivity as an OnPreferenceChangedListener to avoid any memory leaks.
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    /**
     * Methods for setting up the menu
     **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Use AppCompatActivity's method getMenuInflater to get a handle on the menu inflater */
        MenuInflater inflater = getMenuInflater();
        /* Use the inflater's inflate method to inflate our visualizer_menu layout to this menu */
        inflater.inflate(R.menu.ppg_app_menu, menu);
        /* Return true so that the visualizer_menu is displayed in the Toolbar */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_setting) {
            Intent startSettingsActivity = new Intent(this, SettingActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupSharedPreferences() {
        // Get all of the values from shared preferences to set it up
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Logging for Debugging purpose
        boolean doColorCorrection = sharedPreferences.getBoolean(getString(R.string.pref_color_corection_key),
                getResources().getBoolean(R.bool.pref_do_color_correction_default));
        Log.i(TAG, "Do Color Correction is " + doColorCorrection );

        // Register the listener
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    // Updates the screen if the shared preferences change. This method is required when you make a
    // class implement OnSharedPreferenceChangedListener
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(R.string.pref_color_corection_key)){
            //Logging for Debugging purpose
            boolean doColorCorrection = sharedPreferences.getBoolean(
                    key,
                    getResources().getBoolean(R.bool.pref_do_color_correction_default));
            Log.i(TAG, "Do Color Correction is " + doColorCorrection );
        }
    }

    private void showCameraPreview(){
        camView = new SurfaceView(this);
        camHolder = camView.getHolder();
        camPreview = new CameraPreview(this, CameraFrameWidth, CameraFrameHeight);
        camHolder.addCallback(camPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        cameraView = (FrameLayout) findViewById(R.id.frameLayout1);
        cameraView.addView(camView, new ViewGroup.LayoutParams(
                getResources().getInteger(R.integer.preview_frame_size_width),
                getResources().getInteger(R.integer.preview_frame_size_height)));

        stoppingHandler.postDelayed(stopCollectingData, 120000);

        buttonStop = (Button) findViewById(R.id.button_stop);
        buttonStop.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                if (!camPreview.isStopped){
                    camPreview.isStopped = true;
                    camView.getHolder().removeCallback(camPreview);
                    camPreview.stopPreview();
                    camPreview.stopWorkerThread();
                    buttonStop.setText(getString(R.string.button_stopped));
                }
            }
        });

        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                recreateActivityCompat(MyCamera.this);
            }
        });

        buttonTakePicture = (Button) findViewById(R.id.button_take_picture_activity);
        buttonTakePicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyCamera.this,ImageCapturingActivity.class);
                startActivity(intent);
            }
        });

        //Utility.saveMatrix(this,Utility.convertingMat2);
        //Log.i(TAG,testCorrectionMatrixSharedPreferences(this));
        //double[][] testArray = Utility.loadMatrix(this);
        //for (int i = 0; i < 4; i++){
        //    for (int j = 0; j < 3; j++){
        //        Log.i(TAG,"" + testArray[i][j]);
        //    }
        //}
    }

    private void requestPermission(){
        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (!camPreview.isStopped){
            camPreview.isStopped = true;
            camView.getHolder().removeCallback(camPreview);
            camPreview.stopPreview();
            camPreview.stopWorkerThread();
        }
    }

    private Runnable stopCollectingData = new Runnable() {
        @Override
        public void run() {
            if (!camPreview.isStopped){
                camPreview.isStopped = true;
                camView.getHolder().removeCallback(camPreview);
                camPreview.stopPreview();
                camPreview.stopWorkerThread();

            }
        }
    };

    /**
     Current Activity instance will go through its lifecycle to onDestroy() and a new instance then created after it.
     */
    @SuppressLint("NewApi")
    public static final void recreateActivityCompat(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            activity.recreate();
        } else {
            final Intent intent = activity.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            activity.finish();
            activity.overridePendingTransition(0, 0);
            activity.startActivity(intent);
            activity.overridePendingTransition(0, 0);
        }
    }

    public static String testCorrectionMatrixSharedPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_color_correction_matrix_key), "0,0,0;0,0,0;0,0,0;0,0,0");
    }

}


