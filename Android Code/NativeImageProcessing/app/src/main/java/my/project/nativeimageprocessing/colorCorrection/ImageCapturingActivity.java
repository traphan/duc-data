package my.project.nativeimageprocessing.colorCorrection;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import my.project.nativeimageprocessing.CameraPreview;
import my.project.nativeimageprocessing.R;

public class ImageCapturingActivity extends AppCompatActivity {
    Button takePictureButton;
    Button correctionMatrixActivityButton;

    private CameraPreview camPreview;
    private FrameLayout   framePreview;
    private SurfaceView camView;
    private SurfaceHolder camHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_capturing);

        ActionBar actionBar = this.getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        camView = new SurfaceView(this);
        camHolder = camView.getHolder();
        camPreview = new CameraPreview(this,
                getResources().getInteger(R.integer.camera_frame_size_width_color_correction),
                getResources().getInteger(R.integer.camera_frame_size_height_color_correction));

        camHolder.addCallback(camPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        framePreview = (FrameLayout) findViewById(R.id.image_preview_frame);
        framePreview.addView(camView,
                new ViewGroup.LayoutParams(getResources().getInteger(R.integer.preview_frame_size_width_color_correction),
                        getResources().getInteger(R.integer.preview_frame_size_height_color_correction)));

        //Capture still image when clicking button
        takePictureButton = (Button) findViewById(R.id.button_take_picture);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dispatchTakePictureIntent();
                camPreview.requestPreviewFrame();
            }
        });

        correctionMatrixActivityButton = (Button) findViewById(R.id.button_CorrectionMatrixActivity);
        correctionMatrixActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(ImageCapturingActivity.this,CorrectionMatrixCalculatingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }
}
