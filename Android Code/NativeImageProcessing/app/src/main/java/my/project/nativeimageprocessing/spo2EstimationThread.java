package my.project.nativeimageprocessing;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ducdang on 2/21/17.
 */

public class spo2EstimationThread extends HandlerThread {
    private static final String TAG = spo2EstimationThread.class.getSimpleName();
    private Handler mWorkerHandler;
    private Handler mRespondHandler;
    private List<Double> spo2Data;
    private spo2Callback mCallBack;


    public interface spo2Callback {
        void onNewSpo2Value(double newSpo2Value);
    }

    public spo2EstimationThread(Handler respondHandler, spo2EstimationThread.spo2Callback callBack) {
        super(TAG);
        spo2Data = new CopyOnWriteArrayList<>();
        mRespondHandler = respondHandler;
        mCallBack = callBack;
    }

    public void queueTask(int what, double[][] ppgData) {
        //count = 0;
        Log.i(TAG, "New Data section added");
        mWorkerHandler.obtainMessage(what, ppgData).sendToTarget();
    }

    public void prepareHandler() {
        mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                //count = msg.what;
                double[][] ppgData2D = (double[][]) msg.obj;
                double[] ppgRed = ppgData2D[0];
                double[] ppgGreen = ppgData2D[1];
                final double spo2 = nativeSpo2Calculation(ppgRed, ppgGreen);
                Log.d(TAG, "New Data Section Red Size: " + ppgRed.length );
                Log.d(TAG, "New Data Section Green Size: " + ppgGreen.length );
                Log.d(TAG, "Spo2: " + spo2 );

                //Log.d(TAG, "Heartrate: " + heartRate );

                mRespondHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.onNewSpo2Value(spo2);
                    }
                });
                return true;
            }
        });
    }

    private void toSpo2DataFile(){
        try {
            String dataFileName = "spo2.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "MyCamera";
            final String dataPath = MyDirectory_path + "/" + dataFileName;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }
            File dataFile = new File(dataPath);
            FileWriter writer = new FileWriter(dataFile);
            Iterator<Double> iterator = spo2Data.iterator();
            while (iterator.hasNext()) {
                Double spo2 = iterator.next();
                writer.append(spo2.toString());
                writer.write("\n");
            }
            Log.d(TAG, "Data stored at: " + MyDirectory_path);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public native double nativeSpo2Calculation(double[] dataRed, double[] dataGreen);

}
