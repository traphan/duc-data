package my.project.nativeimageprocessing;

/**
 * Created by ducdang on 10/24/16.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import org.opencv.core.Mat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ImageProcessingThread extends HandlerThread {

    private Handler mWorkerHandler;
    private Handler mRespondHandler;
    private static final String TAG = ImageProcessingThread.class.getSimpleName();
    private List<Double> average;
    private int count;
    private int count2 = 0;
    private Callback mCallBack;
    private boolean doColorCorrection;
    private Mat correctionMat;

    public interface Callback {
         void onNewValue(double[] newValue);
    }

    public ImageProcessingThread(Context context, Handler respondHandler, Callback callBack) {
        super(TAG);
        average = new CopyOnWriteArrayList<>();
        mRespondHandler = respondHandler;
        mCallBack = callBack;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        doColorCorrection = prefs.getBoolean(
                context.getString(R.string.pref_color_corection_key),
                context.getResources().getBoolean(R.bool.pref_do_color_correction_default));

        if (doColorCorrection){
            correctionMat = Utility.loadCorrectionMatrix(context);
            Log.i(TAG, "correctionMat " + correctionMat.dump());
        }
    }

    public void queueTask(int count, int PreviewSizeWidth, int PreviewSizeHeight, byte[] data) {
        //count = 0;
        Log.i(TAG, "New frame queued");
        mWorkerHandler.obtainMessage(count, PreviewSizeWidth, PreviewSizeHeight, data).sendToTarget();
    }

    public void prepareHandler() {
        mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                count = msg.what;
                int PreviewSizeWidth = msg.arg1;
                int PreviewSizeHeight = msg.arg2;
                byte[] data = (byte[]) msg.obj;
                count2++;
                //Log.d(TAG, "Width: " + PreviewSizeWidth );
                //Log.d(TAG, "Height: " + PreviewSizeHeight );

                //int[] dataRGB = YUVtoRGB(data,PreviewSizeWidth,PreviewSizeHeight);
                //Mat capturedMatRGB = new Mat(PreviewSizeHeight, PreviewSizeWidth, CvType.CV_32S );
                //capturedMatRGB.put(0, 0, dataRGB);

                //Mat correctedMat = new Mat();
                double[] averageValues;
                //double[] averageValues = calculatingPPG(data, PreviewSizeWidth, PreviewSizeHeight);
                if (doColorCorrection){
                    averageValues = calculatingPPGColorCorrected(data, PreviewSizeWidth, PreviewSizeHeight, correctionMat.getNativeObjAddr());
                    //averageValues = calculatingPPG(data, PreviewSizeWidth, PreviewSizeHeight);

                }else{
                    averageValues = calculatingPPG(data, PreviewSizeWidth, PreviewSizeHeight);
                }
                double tempResultRed = (-1)*averageValues[0];
                double tempResultGreen = (-1)*averageValues[1];
                double tempResultBlue = averageValues[2];

                final double[] ppgResult = {tempResultRed, tempResultGreen};

                //correctedMat.release();
                average.add(tempResultGreen);
                Log.d(TAG, "Average Red: " + tempResultRed );
                Log.d(TAG, "Average Green: " + tempResultGreen );
                Log.d(TAG, "Average Blue: " + tempResultBlue );

                mRespondHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.onNewValue(ppgResult);
                    }
                });
                return true;
            }
        });
    }

    private void toDataFile(){
        try {
            String dataFileName = "data_final.txt";
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String MyDirectory_path = extStorageDirectory + "/" + "PPGapp";
            final String dataPath = MyDirectory_path + "/" + dataFileName;

            File dataFolder = new File(MyDirectory_path);
            if (!dataFolder.isDirectory() && !dataFolder.mkdirs()) {
                Log.e(TAG, "Failed to create app data directory at " + MyDirectory_path);
            }

            File dataFile = new File(dataPath);
            FileWriter writer = new FileWriter(dataFile);
            Iterator<Double> iterator = average.iterator();
            while (iterator.hasNext()) {
                Double avg = iterator.next();
                writer.append(avg.toString());
                writer.write("\n");
            }
            Log.d(TAG, "Data stored at: " + MyDirectory_path);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean quitSafely (){
        while (mWorkerHandler.hasMessages(count) ){
            Log.d(TAG, "Frame Number: " + count2 );
        }
        toDataFile();
        return super.quitSafely();
    }

    private int[] YUVtoRGB(byte[] data, int width, int height){
        int size = width*height;
        int u,v,y1,y2,y3,y4;
        int[] result = new int[size];

        for(int i = 0, k = 0; i < size; i+=2, k+=2){
            y1 = data[i] & 0xff;
            y2 = data[i+1] & 0xff;
            y3 = data[width+i] & 0xff;
            y4 = data[width+i+1] & 0xff;

            v = data[size+k] & 0xff;
            u = data[size+k+1] & 0xff;
            v = v - 128;
            u = u - 128;

            result[i]         = convertYUVtoRGB(y1, u, v);
            result[i+1]       = convertYUVtoRGB(y2, u, v);
            result[width+i]   = convertYUVtoRGB(y3, u, v);
            result[width+i+1] = convertYUVtoRGB(y4, u, v);

            if (i!=0 && (i+2)%width == 0){
                i += width;
            }
        }
        return result;
    }

    private int convertYUVtoRGB(int y, int u, int v){
        int r,g,b;
        r = y + (int)(1.402f*u);
        g = y - (int)(0.344f*v + 0.714f*u);
        b = y + (int)(1.772f*v);

        r = r >255? 255 : r<0 ? 0 : r;
        g = g >255? 255 : g<0 ? 0 : g;
        b = b >255? 255 : b<0 ? 0 : b;

        return 0xff000000 | (r<<16) | (g<<8) | b;
    }

    public native double[] calculatingPPG(byte[] data, int width, int height);

    public native double[] calculatingPPGColorCorrected( byte[] data, int  frameWidth, int frameHeight, long corrMatAdd);
}