//
// Created by ducdang on 4/12/17.
//
#include <jni.h>
#include "matrix.h"
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
extern "C" {
    JNIEXPORT void JNICALL Java_my_project_nativeimageprocessing_colorCorrection_CorrectionMatrixCalculatingActivity_getCorrectionMatrix (JNIEnv * env, jobject obj,
                             jlong matAddrRefChecker, jlong matAddrCapturedChecker, jlong matAddrGeneratedChecker, jlong matAddrCorrectionMat, jlong matAddrCorrected ) {
        Mat &refColorChecker = *(Mat *) matAddrRefChecker;
        Mat &capturedColorChecker = *(Mat *) matAddrCapturedChecker;
        Mat &genColorChecker = *(Mat *) matAddrGeneratedChecker;
        Mat &correctionMatrix = *(Mat *) matAddrCorrectionMat;
        Mat &correctedImage = *(Mat *) matAddrCorrected;

        genColorChecker = generateColorChecker(capturedColorChecker);
        correctionMatrix = generateCorrectionMatrix(refColorChecker, genColorChecker);
    }

    Mat generateCorrectionMatrix(Mat refColorChecker, Mat generatedColorChecker) {
        //Mat& mOrig = *(Mat*)matAddrOrig;
        //Mat& mProg = *(Mat*)matAddrProg;
        //Mat& mConvert = *(Mat*)matAddrConverting;
        //Mat& correctedImage  = *(Mat*)matAddrCorrected;

        vector<Mat> planes;
        split(refColorChecker, planes);

        Mat redTransposed   = planes[1].t();
        Mat redColumn       = redTransposed.reshape(1, 1).t();
        Mat greenTransposed = planes[2].t();
        Mat greenColumn     = greenTransposed.reshape(1, 1).t();
        Mat blueTransposed  = planes[3].t();
        Mat blueColumn      = blueTransposed.reshape(1, 1).t();

        Mat matReshapedOrig = Mat(476 * 320, 3, CV_64F);
        redColumn.col(0).copyTo(matReshapedOrig.col(0));
        greenColumn.col(0).copyTo(matReshapedOrig.col(1));
        blueColumn.col(0).copyTo(matReshapedOrig.col(2));

        split(generatedColorChecker, planes);

        redTransposed   = planes[1].t();
        redColumn       = redTransposed.reshape(1, 1).t();
        greenTransposed = planes[2].t();
        greenColumn     = greenTransposed.reshape(1, 1).t();
        blueTransposed  = planes[3].t();
        blueColumn      = blueTransposed.reshape(1, 1).t();
        Mat onesColumns = Mat::ones(476 * 320, 1, redColumn.type());

        Mat matReshapedProg = Mat(476 * 320, 4, CV_64F);
        onesColumns.col(0).copyTo(matReshapedProg.col(0));
        redColumn.col(0).copyTo(matReshapedProg.col(1));
        greenColumn.col(0).copyTo(matReshapedProg.col(2));
        blueColumn.col(0).copyTo(matReshapedProg.col(3));

        Mat correctionMatrix = Mat(4, 3, CV_64FC1);
        solve(matReshapedProg, matReshapedOrig, correctionMatrix, DECOMP_QR);

        return correctionMatrix;
    }

    Mat generateColorChecker(Mat colorChecker) {
        int rectWidth = 70;
        int rectHeight = 70;
        Mat patch1  = colorChecker(Rect(100, 150, rectWidth, rectHeight));
        Mat patch2  = colorChecker(Rect(300, 150, rectWidth, rectHeight));
        Mat patch3  = colorChecker(Rect(500, 150, rectWidth, rectHeight));
        Mat patch4  = colorChecker(Rect(700, 150, rectWidth, rectHeight));
        Mat patch5  = colorChecker(Rect(900, 150, rectWidth, rectHeight));
        Mat patch6  = colorChecker(Rect(1100,150, rectWidth, rectHeight));
        Mat patch7  = colorChecker(Rect(100, 350, rectWidth, rectHeight));
        Mat patch8  = colorChecker(Rect(300, 350, rectWidth, rectHeight));
        Mat patch9  = colorChecker(Rect(500, 350, rectWidth, rectHeight));
        Mat patch10 = colorChecker(Rect(700, 350, rectWidth, rectHeight));
        Mat patch11 = colorChecker(Rect(900, 350, rectWidth, rectHeight));
        Mat patch12 = colorChecker(Rect(1100,350, rectWidth, rectHeight));
        Mat patch13 = colorChecker(Rect(100, 550, rectWidth, rectHeight));
        Mat patch14 = colorChecker(Rect(300, 550, rectWidth, rectHeight));
        Mat patch15 = colorChecker(Rect(500, 550, rectWidth, rectHeight));
        Mat patch16 = colorChecker(Rect(700, 550, rectWidth, rectHeight));
        Mat patch17 = colorChecker(Rect(900, 550, rectWidth, rectHeight));
        Mat patch18 = colorChecker(Rect(1100,550, rectWidth, rectHeight));
        Mat patch19 = colorChecker(Rect(100, 750, rectWidth, rectHeight));
        Mat patch20 = colorChecker(Rect(300, 750, rectWidth, rectHeight));
        Mat patch21 = colorChecker(Rect(500, 750, rectWidth, rectHeight));
        Mat patch22 = colorChecker(Rect(700, 750, rectWidth, rectHeight));
        Mat patch23 = colorChecker(Rect(900, 750, rectWidth, rectHeight));
        Mat patch24 = colorChecker(Rect(1100,750, rectWidth, rectHeight));

        int generatedColorCheckerWidth = 476;
        int generatedColorCheckerHeight = 320;

        Mat generatedColorChecker = Mat::zeros(generatedColorCheckerHeight, generatedColorCheckerWidth, CV_8UC4);

        patch1.copyTo(generatedColorChecker(Rect(9, 9, rectWidth, rectHeight)));
        patch2.copyTo(generatedColorChecker(Rect(87, 9, rectWidth, rectHeight)));
        patch3.copyTo(generatedColorChecker(Rect(165, 9, rectWidth, rectHeight)));
        patch4.copyTo(generatedColorChecker(Rect(243, 9, rectWidth, rectHeight)));
        patch5.copyTo(generatedColorChecker(Rect(321, 9, rectWidth, rectHeight)));
        patch6.copyTo(generatedColorChecker(Rect(399, 9, rectWidth, rectHeight)));
        patch7.copyTo(generatedColorChecker(Rect(9, 87, rectWidth, rectHeight)));
        patch8.copyTo(generatedColorChecker(Rect(87, 87, rectWidth, rectHeight)));
        patch9.copyTo(generatedColorChecker(Rect(165, 87, rectWidth, rectHeight)));
        patch10.copyTo(generatedColorChecker(Rect(243, 87, rectWidth, rectHeight)));
        patch11.copyTo(generatedColorChecker(Rect(321, 87, rectWidth, rectHeight)));
        patch12.copyTo(generatedColorChecker(Rect(399, 87, rectWidth, rectHeight)));
        patch13.copyTo(generatedColorChecker(Rect(9, 165, rectWidth, rectHeight)));
        patch14.copyTo(generatedColorChecker(Rect(87, 165, rectWidth, rectHeight)));
        patch15.copyTo(generatedColorChecker(Rect(165, 165, rectWidth, rectHeight)));
        patch16.copyTo(generatedColorChecker(Rect(243, 165, rectWidth, rectHeight)));
        patch17.copyTo(generatedColorChecker(Rect(321, 165, rectWidth, rectHeight)));
        patch18.copyTo(generatedColorChecker(Rect(399, 165, rectWidth, rectHeight)));
        patch19.copyTo(generatedColorChecker(Rect(9, 243, rectWidth, rectHeight)));
        patch20.copyTo(generatedColorChecker(Rect(87, 243, rectWidth, rectHeight)));
        patch21.copyTo(generatedColorChecker(Rect(165, 243, rectWidth, rectHeight)));
        patch22.copyTo(generatedColorChecker(Rect(243, 243, rectWidth, rectHeight)));
        patch23.copyTo(generatedColorChecker(Rect(321, 243, rectWidth, rectHeight)));
        patch24.copyTo(generatedColorChecker(Rect(399, 243, rectWidth, rectHeight)));

        return generatedColorChecker;
        //return patch1;
    }

}