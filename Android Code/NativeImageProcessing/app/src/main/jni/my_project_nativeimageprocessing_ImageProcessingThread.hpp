#include <jni.h>
#include <vector>
/* Header for class my_project_nativeimageprocessing_MainActivity */

#ifndef _Included_my_project_nativeimageprocessing_ImageProcessingThread
#define _Included_my_project_nativeimageprocessing_ImageProcessingThread

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jdoubleArray JNICALL Java_my_project_nativeimageprocessing_ImageProcessingThread_calculatingPPG
  (JNIEnv *, jobject, jbyteArray data, jint frameWidth, jint frameHeight);

JNIEXPORT jintArray JNICALL Java_my_project_nativeimageprocessing_CameraPreview_YUVImageToRGBImage
  (JNIEnv *, jobject, jbyteArray data, jint frameWidth, jint frameHeight);

JNIEXPORT jdoubleArray JNICALL Java_my_project_nativeimageprocessing_ImageProcessingThread_calculatingPPGColorCorrected
  (JNIEnv *, jobject, jbyteArray data, jint frameWidth, jint frameHeight, jlong corrMatAdd);

void YUVToRGBImage( jbyte * cData, jint frameWidth, jint frameHeight, int result[]);
int calculatingGreen(int y, int u, int v);
int calculatingRed(int y, int u, int v);
int calculatingBlue(int y, int u, int v);
int convertYUVtoRGB(int y, int u, int v);

#ifdef __cplusplus
}
#endif
#endif
