
#include "heartRateCalculator.h"
#include "testfunction.h"
#include <jni.h>
//#include <assert.h>
//#include <algorithm>
//#include <iostream>
//#include <iterator>
#include "persistence1d.hpp"
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


JNIEXPORT jdouble JNICALL Java_my_project_nativeimageprocessing_heartRateEstimationThread_nativeHeartRateCalculation(JNIEnv *env, jobject obj, jdoubleArray ppgArray) {
    //Convert ppgArray to ppgVector
    jboolean  isCopy;
    jsize size = env->GetArrayLength(ppgArray);
    std::vector<double> ppgVector(0);
    jdouble * temp = env->GetDoubleArrayElements(ppgArray,&isCopy);
    //std::vector<double> ppgVector(temp, temp + sizeof(temp)/sizeof (temp[0]));
    for (int i = 0; i < size; i++){
        ppgVector.push_back(temp[i]);
    }

    //env->GetDoubleArrayRegion(ppgArray,0,size,&ppgVector);
    // Calculate Heart Rate and return
    if (ppgVector.size()==100){
        return testfunction(ppgVector);
        //return ppgVector.size();
    }

    return -1;
}