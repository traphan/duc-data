
#include <jni.h>

#ifndef NATIVEIMAGEPROCESSING_HEARTRATECALCULATOR_H
#define NATIVEIMAGEPROCESSING_HEARTRATECALCULATOR_H
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jdouble JNICALL Java_my_project_nativeimageprocessing_heartRateEstimationThread_nativeHeartRateCalculation(JNIEnv *env,
                                                                                                                     jobject obj, jdoubleArray data);
#ifdef __cplusplus
}
#endif
#endif //NATIVEIMAGEPROCESSING_HEARTRATECALCULATOR_H
