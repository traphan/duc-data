
#include <opencv2/opencv.hpp>
#include <set>
#include "persistence1d.hpp"
#include "testfunction.h"
#include "Wavelet.hpp"
#include "Assertion.hpp"
#include "Exception.hpp"
#include "FPWrap.hpp"
#include "WTFilter.hpp"
using namespace p1d;
using namespace std;
using namespace cv;

void cvPolyfit(cv::Mat &src_x, cv::Mat &src_y, cv::Mat &dst, int order){
    CV_FUNCNAME("cvPolyfit");
    __CV_BEGIN__;
    {
        CV_ASSERT((src_x.rows>0)&&(src_y.rows>0)&&(src_x.cols==1)&&(src_y.cols==1)
                  &&(dst.cols==1)&&(dst.rows==(order+1))&&(order>=1));
        Mat X;
        X = Mat::zeros(src_x.rows, order+1,CV_64FC1);
        Mat copy;
        for(int i = 0; i <=order;i++)
        {
            copy = src_x.clone();
            pow(copy,i,copy);
            Mat M1 = X.col(i);
            copy.col(0).copyTo(M1);
        }
        Mat X_t, X_inv;
        transpose(X,X_t);
        Mat temp = X_t*X;
        Mat temp2;
        invert (temp,temp2);
        Mat temp3 = temp2*X_t;
        Mat W = temp3*src_y;
        dst = W.clone();
    }
    __CV_END__;
}

void polyval(cv::Mat &params, cv::Mat &input , cv::Mat &result) {

    Mat copy;
    result = Mat::zeros(input.rows,1,CV_64FC1);
    for(int i = 0; i < params.rows ;i++){
        copy = input.clone();
        pow(copy,i,copy);
        result = result + copy*params.at<double>(0,i);
    }
}

void useAPI(std::vector<float>& x, std::size_t outputSize, std::vector<float>& outVector) {
    // Create the requested wavelet filter and scaling filter
    WT::Filter::FType filterType = WT::Filter::Haar;
    WT::Filter::WaveletFilter wavefilt = WT::Filter::getFilters(filterType).first;    //e
    WT::Filter::ScalingFilter scalefilt = WT::Filter::getFilters(filterType).second;//e
    // Locals
    int maxLevel = 3;
    std::string prefix = "";
    bool useStdout = false;

    WT::SaveLastLevel<float> vop2(maxLevel); // retain values of last level //e
    // Wavelet coefficient related operations
    WT::DoNothing wop0;
    //Smoothing signal
    WT::modwt(x, wavefilt, scalefilt, maxLevel, vop2, wop0); //e
    smooth2(vop2.Values(), scalefilt, maxLevel ,outputSize, outVector); //e
}


float testfunction(std::vector<double>& signalPart){

    //Detrending Signal
    vector<double> index;
    for (int i = 0; i<100; i++){
        index.push_back(i);
    }
    Mat signalMat(signalPart, false);
    Mat indexMat(index,false);
    int fitOrder = 3;
    Mat fit_weights(fitOrder+1,1,CV_64FC1);
    cvPolyfit(indexMat,signalMat,fit_weights,fitOrder);
    Mat signalTrend;
    polyval(fit_weights,indexMat,signalTrend);
    Mat detrendedMat = signalMat - signalTrend;
    //End Detrending signal

    // Convert Mat type data to vector<float>
    vector<float> detrendedSignal ;
    if (detrendedMat.isContinuous()) {
        detrendedSignal.assign((double*)detrendedMat.datastart, (double*)detrendedMat.dataend);
    } else {
        for (int i = 0; i < detrendedMat.rows; ++i) {
            detrendedSignal.insert(detrendedSignal.end(), (double*)detrendedMat.ptr<uchar>(i), (double*)detrendedMat.ptr<uchar>(i)+detrendedMat.cols);
        }
    }
    // End converting Mat dta to float

    //Filtering Detrended signal with wavelet transform-inverse
    std::size_t outputSize = detrendedSignal.size();
    vector<float> filteredSignal;
    useAPI(detrendedSignal, outputSize, filteredSignal);
    // End filtering step

    // Peak detection  
    Persistence1D p;
    vector<int> min, max;
    bool enableMatlabIndexing = true;
    float filterThreshold = 0.1;
    p.RunPersistence(filteredSignal);
    if (!p.VerifyResults()) {
        //cout << "ERROR" << endl;
    }

    p.GetExtremaIndices(min, max, filterThreshold, enableMatlabIndexing);
    int globalMinIndex = p.GetGlobalMinimumIndex(enableMatlabIndexing);
    // End peak detection

    // Heartrate calculation
    std::set<int> maxSet;
    std::pair<std::set<int>::iterator,bool> ret;
    for (int i=0; i< max.size(); ++i) {
        maxSet.insert(max[i]);
    }
    int firstPeak = *maxSet.cbegin();
    int lastPeak = *(--maxSet.cend());
    if (lastPeak != firstPeak){
        float heartrate = ((maxSet.size() -1)*(30*60))/(lastPeak - firstPeak);
        return heartrate;
    }
    // End Heartrate calculation
    return 0;
}

float spo2(std::vector<double>& signalPartGreen, std::vector<double>& signalPartRed){
    //Detrending Signal
    vector<double> indexG;
    for (int i = 0; i<100; i++){
        indexG.push_back(i);
    }
    vector<double> indexR;
    for (int i = 0; i<100; i++){
        indexR.push_back(i);
    }

    Mat signalMatGreen(signalPartGreen, false);
    Mat signalMatRed(signalPartRed, false);
    Mat indexMatGreen(indexG,false);
    Mat indexMatRed(indexR,false);
    int fitOrder = 3;
    Mat fit_weightsGreen(fitOrder+1,1,CV_64FC1);
    Mat fit_weightsRed(fitOrder+1,1,CV_64FC1);
    cvPolyfit(indexMatGreen,signalMatGreen,fit_weightsGreen,fitOrder);
    cvPolyfit(indexMatRed,signalMatRed,fit_weightsRed,fitOrder);

    Mat signalTrendGreen;
    Mat signalTrendRed;
    polyval(fit_weightsGreen,indexMatGreen,signalTrendGreen);
    polyval(fit_weightsRed,indexMatRed,signalTrendRed);
    Mat detrendedMatGreen = signalMatGreen - signalTrendGreen;
    Mat detrendedMatRed = signalMatRed - signalTrendRed;
    //End Detrending signal



    // Convert Mat type data to vector<float>
    vector<float> detrendedSignalGreen ;
    vector<float> detrendedSignalRed ;
    if (detrendedMatGreen.isContinuous()) {
        detrendedSignalGreen.assign((double*)detrendedMatGreen.datastart, (double*)detrendedMatGreen.dataend);
    } else {
        for (int i = 0; i < detrendedMatGreen.rows; ++i) {
            detrendedSignalGreen.insert(detrendedSignalGreen.end(), (double*)detrendedMatGreen.ptr<uchar>(i), 					(double*)detrendedMatGreen.ptr<uchar>(i)+detrendedMatGreen.cols);
        }
    }
    if (detrendedMatRed.isContinuous()) {
        detrendedSignalRed.assign((double*)detrendedMatRed.datastart, (double*)detrendedMatRed.dataend);
    } else {
        for (int i = 0; i < detrendedMatRed.rows; ++i) {
            detrendedSignalRed.insert(detrendedSignalRed.end(), (double*)detrendedMatRed.ptr<uchar>(i), 					(double*)detrendedMatRed.ptr<uchar>(i)+detrendedMatRed.cols);
        }
    }
    // End converting Mat data to float

    // Peak detection
    Persistence1D pGreen;
    Persistence1D pRed;
    vector<int> minGreen, maxGreen, minRed, maxRed;
    bool enableMatlabIndexing = false;
    float filterThresholdGreen = 0.23;
    float filterThresholdRed = 0.1;
    pGreen.RunPersistence(detrendedSignalGreen);
    pRed.RunPersistence(detrendedSignalRed);
    if (!pGreen.VerifyResults() || !pRed.VerifyResults())
    {
        //cout << "ERROR" << endl;
    }
    pGreen.GetExtremaIndices(minGreen, maxGreen, filterThresholdGreen, enableMatlabIndexing);
    pRed.GetExtremaIndices(minRed, maxRed, filterThresholdRed, enableMatlabIndexing);
    int globalMinIndexGreen = pGreen.GetGlobalMinimumIndex(enableMatlabIndexing);
    int globalMinIndexRed = pRed.GetGlobalMinimumIndex(enableMatlabIndexing);
    minGreen.push_back(globalMinIndexGreen);
    minRed.push_back(globalMinIndexRed);
    // End peak detection

    // SpO2 calculation
    std::set<int> maxSetGreen;
    std::set<int> minSetGreen;
    std::set<int> maxSetRed;
    std::set<int> minSetRed;
    std::vector<float> sqrtTermVecRed;
    std::vector<float> sqrtTermVecGreen;
    std::vector<float> spo2vec;

    float eHb600 = 15;
    float eHbO600 = 3.24;
    float eHbHbodiff600 = eHb600 - eHbO600;
    float eHb940 = 0.735;
    float eHbO940 = 1.32;
    float eHbHbodiff940 = eHb940 - eHbO940;

    std::set<int>::iterator it;
    for (int i=0; i< maxGreen.size(); ++i) {
        maxSetGreen.insert(maxGreen[i]);
    }
    for (int i=0; i< minGreen.size(); ++i) {
        minSetGreen.insert(minGreen[i]);
    }
    for (int i=0; i< maxRed.size(); ++i) {
        maxSetRed.insert(maxRed[i]);
    }
    for (int i=0; i< minRed.size(); ++i) {
        minSetRed.insert(minRed[i]);
    }

    // Calculating slope and bottom-to-peak height
    std::set<int>::iterator itMinGreen, itMaxGreen, itMinRed, itMaxRed;
    itMinGreen = minSetGreen.begin();
    for (itMaxGreen = maxSetGreen.begin(); itMaxGreen != maxSetGreen.end();  itMaxGreen++){
        float peakBottomHeightGreen = (detrendedSignalGreen[*itMaxGreen] - detrendedSignalGreen[*itMinGreen])*1000;
        float slopeGreen = 30*peakBottomHeightGreen/(*itMaxGreen - *itMinGreen);
        float sqrtTermGreen = sqrt(slopeGreen*log(peakBottomHeightGreen));
        sqrtTermVecGreen.push_back(sqrtTermGreen);
        itMinGreen++;
    }
    itMinRed = minSetRed.begin();
    for (itMaxRed = maxSetRed.begin(); itMaxRed != maxSetRed.end();  itMaxRed++){
        float peakBottomHeightRed = (detrendedSignalRed[*itMaxRed] - detrendedSignalGreen[*itMinRed])*1000;
        float slopeRed = 30*peakBottomHeightRed/(*itMaxRed - *itMinRed);
        float sqrtTermRed = sqrt(slopeRed*log(peakBottomHeightRed));
        sqrtTermVecRed.push_back(sqrtTermRed);
        itMinRed++;
    }
    if(sqrtTermVecRed.size() == sqrtTermVecGreen.size()){
        for(int i = 0; i<sqrtTermVecGreen.size(); i++){
            float spo2 = (eHb600*sqrtTermVecGreen[i] - eHb940*sqrtTermVecRed[i])/(sqrtTermVecGreen[i]*eHbHbodiff600 - sqrtTermVecRed[i]*eHbHbodiff940);
            spo2vec.push_back(spo2);
        }
        float spo2avg = 0;
        for (int i = 0; i<spo2vec.size(); i++){
            spo2avg += spo2vec[i];
        }
        spo2avg = spo2avg/spo2vec.size();
        return spo2avg;
    }
    return 0;
    // End spo2 calculation
}


