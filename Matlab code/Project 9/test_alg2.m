clc; close all; clear all;
orig = imread('chart_classic.png');
prog1 = imread('chart_processed_GalaxyEdge.png');
prog2 = imread('chart_processed_MotoG4.png');


figure; image(orig);
figure; image(prog1);
figure; image(prog2);

patch_info = load('patch_info.mat');
[cal_img1, a1, fit, proc_mean1, cal_mean1, orig_mean1] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog1, 1, 1, 476, 320, 0, 0);
figure; image(cal_img1/255);
[cal_img2, a2, fit, proc_mean2, cal_mean2, orig_mean2] = color_xform(orig, 1, 1, 476, 320, patch_info.patch_info, prog2, 1, 1, 476, 320, 0, 0);
figure; image(cal_img2/255);

close all; 

finalChart1 = refining_checker_generator(320, 476, cal_img1, a1(1,:));
finalChart2 = refining_checker_generator(320, 476, cal_img2, a2(1,:));

imwrite(finalChart1/255,'cal_img_galaxy.png')
imwrite(finalChart2/255,'cal_img_MotoG4.png')

figure
hold on
plot(proc_mean1(:,2));
plot(cal_mean1(:,1),'--');
plot(orig_mean1(:,1));
legend('Ep','Eo');
title('Red')
hold off

figure
hold on
plot(proc_mean1(:,3));
plot(cal_mean1(:,2),'--');
plot(orig_mean1(:,2));
title('Green')
legend('Ep','Eo');
hold off

figure
hold on
plot(proc_mean1(:,4));
plot(cal_mean1(:,3),'--');
plot(orig_mean1(:,3));
title('Blue')
legend('Ep','Eo');
hold off

figure
hold on
plot(proc_mean2(:,2));
plot(cal_mean2(:,1),'--');
plot(orig_mean2(:,1));
title('Red')
legend('Ep','Eo');
hold off

figure
hold on
plot(proc_mean2(:,3));
plot(cal_mean2(:,2),'--');
plot(orig_mean2(:,2));
title('Red')
title('Green')
legend('Ep','Eo');
hold off

figure
hold on
plot(proc_mean2(:,4));
plot(cal_mean2(:,3),'--');
plot(orig_mean2(:,3));
title('Blue')
legend('Ep','Eo');
hold off

figure
hold on
plot(cal_mean1(:,1));
plot(cal_mean2(:,1));
plot(orig_mean2(:,1),'--');
title('Red')
legend('Corrected Motorola G4','Corrected Samsung Galaxy S6 Edge', 'Original');
hold off

figure
hold on
plot(cal_mean1(:,2));
plot(cal_mean2(:,2));
plot(orig_mean2(:,2),'--');
title('Green')
legend('Corrected Motorola G4','Corrected Samsung Galaxy S6 Edge', 'Original');
hold off


figure
hold on
plot(cal_mean1(:,3));
plot(cal_mean2(:,3));
plot(orig_mean2(:,3),'--');
title('Blue')
legend('Corrected Motorola G4','Corrected Samsung Galaxy S6 Edge', 'Original');
hold off

figure
hold on
plot(proc_mean1(:,2));
plot(proc_mean2(:,2));
plot(orig_mean2(:,1),'--');
title('Red')
legend('Uncorrected Motorola G4','Uncorrected Samsung Galaxy S6 Edge', 'Original');
hold off

figure
hold on
plot(proc_mean1(:,3));
plot(proc_mean2(:,3));
plot(orig_mean2(:,2),'--');
title('Green')
legend('Uncorrected Motorola G4','Uncorrected Samsung Galaxy S6 Edge', 'Original');
hold off


figure
hold on
plot(proc_mean1(:,4));
plot(proc_mean2(:,4));
plot(orig_mean2(:,3),'--');
title('Blue')
legend('Uncorrected Motorola G4','Uncorrected Samsung Galaxy S6 Edge', 'Original');
hold off


%figure; image(cal_img/255);