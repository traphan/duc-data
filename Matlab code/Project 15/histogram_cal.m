clc; 
close all; 
clear all;
rawImg = imread('chart_processed_sam_8.png'); % samsung phone patch 8, motophone patch 8
sc_p = 70; % col size of each patch (in pixels)
sr_p = 70; % row size of each patch (in pixels)
image(rawImg);

patch_info = load('patch_info');
patch_info = patch_info.patch_info;
patch_info(:,3) = 69;
patch_info(:,4) = 69;
temp = patch_info(:,1);
patch_info(:,1) = patch_info(:,2);
patch_info(:,2) = temp;
patch = zeros([70 70 3 24]);
for i = 1:24
    patch(:,:,:,i) = imcrop(rawImg, patch_info(i,:));
end


for i = 1:24
    red   = patch(:,:,1,i);
    green = patch(:,:,2,i);
    blue  = patch(:,:,3,i);
    red   = red(:);
    green = green(:);
    blue  = blue(:);
    red_mean       = mean(red);
    r(i) = red_mean;
    green_mean     = mean(green);
    g(i) = green_mean;
    blue_mean      = mean(blue);
    b(i) = blue_mean;
    
    red_dev(i)  = std(double(red))/red_mean;
    green_dev(i)  = std(double(green))/green_mean;
    blue_dev(i)  = std(double(blue));
    
    red_cov(i)  = std(double(red))/red_mean;
    green_cov(i)  = std(double(green))/green_mean;
    blue_cov(i)  = std(double(blue))/blue_mean;
end

%figure;
%hold on;
%plot([243 200 160 122 85 52],[243 200 160 122 85 52]);
%plot([243 200 160 122 85 52],r(19:24));
%plot([243 200 160 122 85 52],g(19:24));
%plot([243 200 160 122 85 52],b(19:24));
%hold off


fileID = fopen('mean_var_moto_noflash.txt','w');
for i = 1:24
    red   = patch(:,:,1,i);
    green = patch(:,:,2,i);
    blue  = patch(:,:,3,i);
    red   = red(:);
    green = green(:);
    blue  = blue(:);
    red_mean       = mean(red);
    red_variance   = var(double(red));
    red_deviation  = std(double(red));
    green_mean     = mean(green);
    green_variance = var(double(green));
    green_deviation  = std(double(green));
    blue_mean      = mean(blue);
    blue_variance  = var(double(blue));
    blue_deviation  = std(double(blue));
    fprintf(fileID,'Patch %6d\n',i);
    fprintf(fileID,'red_mean     = %6.2f\n',red_mean);
    fprintf(fileID,'red_variance = %6.2f\n',red_variance);
    fprintf(fileID,'red_deviation = %6.2f\n',red_deviation);
    fprintf(fileID,'green_mean   = %6.2f\n',green_mean);
    fprintf(fileID,'green_variance = %6.2f\n',green_variance);
    fprintf(fileID,'green_deviation = %6.2f\n',green_deviation);
    fprintf(fileID,'blue_mean      = %6.2f\n',blue_mean);
    fprintf(fileID,'blue_variance  = %6.2f\n',blue_variance);
    fprintf(fileID,'blue_deviation = %6.2f\n',blue_deviation);
    fprintf(fileID,'\n\n\n\n\n');
end
fclose(fileID);

for i = 1:24
    fileName = ['hist_sam_noFlash_',num2str(i),'R_.png'];
    close all;
    figure, histogram(patch(:,:,1,i));
    title('red channel')
    xlim([0 260])
    saveas(gcf,fileName);
    
    fileName = ['hist_sam_noFlash_',num2str(i),'G_.png'];
    close all;
    figure, histogram(patch(:,:,2,i));
    title('Green channel')
    xlim([0 255])
    saveas(gcf,fileName);    
    
    fileName = ['hist_sam_noFlash_',num2str(i),'B_.png'];
    close all;
    figure, histogram(patch(:,:,3,i));
    title('Blue channel')
    xlim([0 255])
    saveas(gcf,fileName);    
end
